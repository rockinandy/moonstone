Moonstone
=========

Moonstone is a container implementation for FreeBSD. It seeks to bring the
values of reproducibility, consistency, portability, and isolation that is being
widely adopted in the Linux ecosystem to FreeBSD, which has the necessary
building blocks for containers.

_Moonstone is very much a work-in-progress. In its current form, it is more of a
proof-of-concept rather than something to run in production. It is under heavy
development currently. Moonstone will follow semantic versioning rules, so
expect stability from 1.0 and onwards._

Getting Started
---------------

Moonstone consists of a single shell script called `moonstone`. Copy that to a
FreeBSD system and mark it as executable:

```
fetch https://bitbucket.org/rockinandy/moonstone/raw/master/bin/moonstone
chmod +x moonstone
```

Unless otherwise specified, you will need to run all of these following commands
as `root`. You can do that with `su` or `sudo` (if you have that installed).

First, you will need to set up the current host to run Moonstone containers by
bootstrapping it:

```
# ./moonstone bootstrap
```

Once that finishes, moonstone is ready to run containers. If you don't have any
containers yet, you can build a plain FreeBSD install image locally with:

```
# ./moonstone freebsd
```

After that completes successfully, you will now have an image (named something
like `freebsd-12.0-release-amd64` to run and build other images upon. You can
see the images that you have locally by running `./moonstone images`. To start a
container from this image, you can run:

```
# ./moonstone create freebsd-12.0-release-amd64
```

When that command completes, it should print a line like:

```
Container f30612dc-6313-11e9-8f13-a8206611fc48 is ready for use
```

You will need that container ID for other commands. I'll reference it as
`$CONTAINER_ID` from now on. You can also have it saved to a file by passing
`--cidfile <filename>` to `create`.

At this point, the container has been created and is ready for use. You can list
the running containers with `./moonstone ps`. To start a shell within the
container, run:

```
# ./moonstone exec $CONTAINER_ID /bin/sh
```

This will drop you into the container so that you can run other commands. Poke
around... it should look like a FreeBSD machine. When you are done, run `exit`
or hit Ctrl-D to exit the container.

You can copy files between the host and container with `./moonstone cp`:

```
# ./moonstone cp /file/on/host $CONTAINER_ID:/file/in/container
```

When you are finished making changes to a container, you can convert that
container into an image with:

```
# ./moonstone stop $CONTAINER_ID
# ./moonstone commit -n <name> $CONTAINER_ID
```

This image will be available locally. If you'd like to export it into an image
file that you can transfer between machines, you can do so with:

```
# ./moonstone export <image>
```

This will create an image archive in your current directory with the name
`<image>.tar`.

To import a previously created image (by you or someone else), you can do so
with:

```
# ./moonstone pull https://example.com/path/to/<image>.tar
```

This will import whatever you have specified for <image> into the local host. It
will recursively fetch any images it depends on too, provided that they are in
the same directory on the remote host.

Finally, when you are done with a container, you can remove it with:

```
# ./moonstone rm $CONTAINER_ID
```

This frees all of the resources associated with that container.


Available Commands
------------------

Moonstone has a number of commands and continues to add more:

```
$ ./moonstone
Moonstone 0.1 - A container implementation for FreeBSD

Usage:
  moonstone <command>

Available Commands:
  bootstrap       Set up Moonstone for use on this system
  freebsd         Create a FreeBSD base image
  images          List images in local storage
  import          Import an image from an archive format
  pull            Pull an image from a remote source
  rmi             Remove an image from local storage
  commit          Create a new image based on the current contents of a container
  export          Export images into an archive format
  ps              List containers
  create          Create a new container without running anything in it
  env             Adds or removes environment variables for a container
  limits          Adds or removes resource limits for a container
  entrypoint      Set or return the entrypoint for a container
  exec            Run a command within a container
  start           Run the entrypoint command within the container
  run             Fetch and run a container
  stop            Stop a running container
  restart         Restart a container
  rm              Remove a previously created container
  cp              Copy files between a container and local storage
```


How it Works
------------

Moonstone uses FreeBSD Jails for process and filesystem isolation. It uses VNET
for network stack isolation, using netgraph to bridge between a host interface
and the jail interface. Resource limits are accomplished with RCTL. ZFS provides
the storage layer for images and containers, making use of its
cloning/copy-on-write capabilities to layer images on top of each other.
